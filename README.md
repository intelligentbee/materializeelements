# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Collection of elements built upon materialize.css framework
* Version 0.1

### How do I get set up? ###

You can see working examples in the demo folder. Use the classes/ids/tags which have been used there

### Contribution guidelines ###

1.Install bower(package manager) -- for materialize and jquery--  and grunt (js task runner) for less and minify.
2.see materialElements.less for how element styles are defined and materialElements.js for their js.

### Who do I talk to? ###

lucian@intelligentbee.com