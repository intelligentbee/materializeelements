/**
 * Created by jedibee-dev on 2/13/2015.
 */
$.fn.topSearch = function () {
    var focus = function (e) {
        $(".headerSearchForm").activateSearch(e);
        $(".headerSearchInput").blur(function () {
            $(".headerSearchForm").deactivateSearch();
        })
    }
    $(this).on('mousedown', focus);
    $(this).on('touchstart', focus);
    $(this).on('click', focus);
};

$.fn.activateSearch = function (e) {
    var input = $(this).find("input");
    if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
        e.stopPropagation();
        e.preventDefault();
        window.setTimeout(function () {
            input.value = input.value || "";
            input.focus();
        }, 10);
    } else {
        input.select();
    }
    $(this).addClass("active");
};

$.fn.deactivateSearch = function () {
    $(this).removeClass("active");
};

$.fn.scrollReact = function () {
    $(this).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $("body").addClass("scrolled");
        } else {
            $("body").removeClass("scrolled");
        }
    })
}