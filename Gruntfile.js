/**
 * Created by jedibee-dev on 2/5/2015.
 */
module.exports = function (grunt) {
    grunt.initConfig({
        less: {
            development: {
                options: {
                    paths: ["bin"]
                },
                files: {
                    "bin/materialElements.css": "less/materialElements.less"
                }
            },
            production: {
                options: {
                    paths: ["bin"],
                    cleancss: true
                },
                files: {
                    "bin/materialElements.css": "less/materialElements.less"
                }
            }
        },
        cssmin: {
            css: {
                src: 'bin/materialElements.css',
                dest: 'bin/materialElements.min.css'
            }
        },
        watch: {
            files: "less/*",
            tasks: ["less", "cssmin"]
        }
    });
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', ['watch'])
};